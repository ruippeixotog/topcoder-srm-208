public class TallPeople {

	public int[] getPeople(String[] people) {
		int[][] matrix = new int[people.length][];
		for (int i = 0; i < people.length; i++) {
			String[] strArr = people[i].split(" ");
			int[] arr = new int[strArr.length];

			for (int j = 0; j < arr.length; j++) {
				arr[j] = Integer.parseInt(strArr[j]);
			}
			matrix[i] = arr;
		}

		int tallest = Integer.MIN_VALUE;
		for (int i = 0; i < matrix.length; i++) {
			int shortest = Integer.MAX_VALUE;
			for (int j = 0; j < matrix[i].length; j++) {
				shortest = Math.min(shortest, matrix[i][j]);
			}
			tallest = Math.max(tallest, shortest);
		}

		int shortest2 = Integer.MAX_VALUE;
		for (int j = 0; j < matrix[0].length; j++) {
			int tallest2 = Integer.MIN_VALUE;
			for (int i = 0; i < matrix.length; i++) {
				tallest2 = Math.max(tallest2, matrix[i][j]);
			}
			shortest2 = Math.min(shortest2, tallest2);
		}

		return new int[] { tallest, shortest2 };
	}
}
